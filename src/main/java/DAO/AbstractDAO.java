package DAO;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connection.*;
import java.util.logging.Logger;
import java.util.logging.Level;


public abstract class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T>  type;

    public AbstractDAO() {
        this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void insert(T object){
        List<String> atributes = getObjectAttributes(object);

        String insertStatementString = "INSERT INTO " + type.getSimpleName() + " VALUES (";

        for (int i = 0; i < atributes.size(); i++){
            insertStatementString = insertStatementString + "'" + atributes.get(i) + "'"
                                        + ((i != atributes.size() - 1) ? ", ":");");
        }

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;

        try {
            insertStatement = connection.prepareStatement(insertStatementString);

            insertStatement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getSimpleName() + "insert: " + e.getMessage());
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(insertStatement);
        }
    }

    public void delete(int id){
        String deleteStatementString = "DELETE FROM " + type.getSimpleName() + " WHERE " + type.getSimpleName()
                                            + "_id=" + id + ";";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try {
            deleteStatement = connection.prepareStatement(deleteStatementString);

            deleteStatement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getSimpleName() + "delete: " + e.getMessage());
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(deleteStatement);
        }
    }

    public void update(T object){
        List<String> atributes = getObjectAttributes(object);
        List<String> atributesName = getObjectAttributesNames(object);

        String updateStatementString = "UPDATE " + type.getSimpleName() + " SET ";

        for (int i = 1; i < atributes.size(); i++){
            updateStatementString = updateStatementString + atributesName.get(i) + "='" +
                                        atributes.get(i) + "'" + ((i == atributes.size() - 1) ? " ":", ");
        }

        updateStatementString += " WHERE " + type.getSimpleName() + "_id=" + atributes.get(0);

        System.out.println(updateStatementString);

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;

        try {
            updateStatement = connection.prepareStatement(updateStatementString);

            updateStatement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getSimpleName() + "insert: " + e.getMessage());
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(updateStatement);
        }
    }

    public List<T> showAll(){
        String showAllStatementString = "SELECT * FROM " + type.getSimpleName() + ";";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement showAllStatement = null;
        ResultSet resultSet = null;

        try{
            showAllStatement = connection.prepareStatement(showAllStatementString);

            resultSet = showAllStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return convertToObjectList(resultSet);
    }

    public List<T> findById(int id){
        String findByIdStatementString = "SELECT * FROM " + type.getSimpleName() + " WHERE " +
                                                type.getSimpleName() + "_id=" + id + ";";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement findByIdStatement = null;
        ResultSet resultSet = null;

        try{
            findByIdStatement = connection.prepareStatement(findByIdStatementString);

            resultSet = findByIdStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return convertToObjectList(resultSet);
    }

    private List<String> getObjectAttributes(T object){
        List<String> toReturn = new ArrayList<String>();

        for (Field f : object.getClass().getDeclaredFields()){
            f.setAccessible(true);

            try {
                toReturn.add("" + f.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return toReturn;
    }

    private List<String> getObjectAttributesNames(T object){
        List<String> toReturn = new ArrayList<String>();

        for (Field f : object.getClass().getDeclaredFields()){
            f.setAccessible(true);
            toReturn.add("" + f.getName());
        }

        return toReturn;
    }

    public List<T> convertToObjectList (ResultSet resultSet){
        List<T> toReturn = new ArrayList<T>();

        try{
            while (resultSet.next()){
                T instance = type.newInstance();

                for (Field f : type.getDeclaredFields()){
                    Object obj = resultSet.getObject(f.getName());
                    PropertyDescriptor pd = new PropertyDescriptor(f.getName(), type);
                    pd.getWriteMethod().invoke(instance, obj);
                }

                toReturn.add(instance);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
        }

        return toReturn;
    }

}
