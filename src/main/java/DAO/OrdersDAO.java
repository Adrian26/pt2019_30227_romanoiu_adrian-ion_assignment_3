package DAO;

import BLL.ProductBLL;
import Connection.*;
import Model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class OrdersDAO extends AbstractDAO<OrdersDAO>{

    private int OrderID;
    private String FirstName;
    private String LastName;
    private String Products;

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getProducts() {
        return Products;
    }

    public void setProducts(String products) {
        Products = products;
    }

    public OrdersDAO(){
        super();
    }

    public List<OrdersDAO> showAll(){
        String showAllStatementString = "SELECT\n" +
                                        "\torders.order_id AS 'OrderID',\n" +
                                        "\tclient.first_name AS 'FirstName',\n" +
                                        "    client.last_name AS 'LastName',\n" +
                                        "    GROUP_CONCAT(product.name) AS 'Products'\n" +
                                        "FROM orders, client, product, order_items \n" +
                                        "WHERE \n" +
                                        "\torders.client_id = client.client_id AND\n" +
                                        "    orders.order_id = order_items.order_id AND\n" +
                                        "\tproduct.product_id=order_items.product_id\n" +
                                        "GROUP BY orders.order_id;";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement showAllStatement = null;
        ResultSet resultSet = null;

        try{
            showAllStatement = connection.prepareStatement(showAllStatementString);

            resultSet = showAllStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return convertToObjectList(resultSet);
    }

    public void insert(int client_id, String[] orderItems){
        String insertStatementString_1 = "INSERT INTO " + "orders" + "  VALUES (0, " + client_id + ");";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;

        try {
            insertStatement = connection.prepareStatement(insertStatementString_1);

            insertStatement.execute();

            String auxString = "SELECT MAX(order_id) AS 'order_id' FROM orders";
            PreparedStatement auxStatement = connection.prepareStatement(auxString);

            ResultSet rs = auxStatement.executeQuery();
            rs.next();
            Integer orderID = (Integer) rs.getObject("order_id");

            for (String s : orderItems){
                String[] items = s.split("-");

                String statement = "INSERT INTO order_items VALUES (" + orderID + ", " + items[0]
                                        + ", " + items[1] + ");";

                List<Product> product = new ProductDAO().findById(Integer.parseInt(items[0]));

                if (product.get(0).getQuantity() >= Integer.parseInt(items[1])){
                    PreparedStatement preparedStatement = connection.prepareStatement(statement);
                    preparedStatement.execute();

                    product.get(0).setQuantity(product.get(0).getQuantity() - Integer.parseInt(items[1]));
                    new ProductBLL().update(product.get(0));
                }
                else{
                    System.out.println("Not enough quantity!");
                }
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Orders-insert: " + e.getMessage());
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(insertStatement);
        }
    }

    public void delete(int id){
        String deleteStatementString = "DELETE FROM orders " + " WHERE "
                + "order_id=" + id + ";";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try {
            deleteStatement = connection.prepareStatement(deleteStatementString);

            deleteStatement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Orders-delete: " + e.getMessage());
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(deleteStatement);
        }
    }

}
