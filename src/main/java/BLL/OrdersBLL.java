package BLL;

import DAO.OrdersDAO;

import java.util.List;

public class OrdersBLL {

    public List<OrdersDAO> showAll(){
        return new OrdersDAO().showAll();
    }

    public void insert(int client_id, String[] orderItems){
        new OrdersDAO().insert(client_id, orderItems);
    }

    public void delete(int id){
        new OrdersDAO().delete(id);
    }
}
