package BLL;

import DAO.ProductDAO;
import Model.Product;

import java.util.List;

public class ProductBLL {

    public void insert(Product c){
        new ProductDAO().insert(c);
    }

    public void delete(int id){
        new ProductDAO().delete(id);
    }

    public List<Product> showAll(){
        return new ProductDAO().showAll();
    }

    public void update (Product c){
        new ProductDAO().update(c);
    }

    public List<Product> findById(int id){
        return new ProductDAO().findById(id);
    }

}
