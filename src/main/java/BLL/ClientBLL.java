package BLL;

import DAO.ClientDAO;
import Model.*;

import java.util.List;

public class ClientBLL {

    public void insert(Client c){
        new ClientDAO().insert(c);
    }

    public void delete(int id){
        new ClientDAO().delete(id);
    }

    public List<Client> showAll(){
        return new ClientDAO().showAll();
    }

    public void update (Client c){
        new ClientDAO().update(c);
    }

    public List<Client> findById(int id){
        return new ClientDAO().findById(id);
    }

}
