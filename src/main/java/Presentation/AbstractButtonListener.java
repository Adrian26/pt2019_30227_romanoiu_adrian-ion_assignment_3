package Presentation;

import BLL.ClientBLL;
import BLL.ProductBLL;
import DAO.ClientDAO;
import DAO.ProductDAO;
import Model.Client;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class AbstractButtonListener implements ActionListener {
    private JFrame frame;

    private JButton addButton = new JButton("Add");
    private JButton deleteButton = new JButton("Delete");
    private JButton updateButton = new JButton("Update Row");

    private JPanel mainPanel = new JPanel(new GridLayout(2, 1));
    private JPanel panel = new JPanel(new GridLayout(1, 3));

    private ActionListener update;
    private ActionListener doneUpdate;

    private JScrollPane scrollPane;

    private JTable table;

    public AbstractButtonListener(Object obj){
        if (obj instanceof Client){
            this.frame = new JFrame("Clients");
            table = new DatabaseTable<Client>().createTable(new ClientDAO().showAll());
        }
        else if (obj instanceof  Product){
            this.frame = new JFrame("Products");
            table = new DatabaseTable<Product>().createTable(new ProductDAO().showAll());
        }

        addButton.addActionListener(new AddButtonListener(obj));
        deleteButton.addActionListener(new DeleteButtonListener(obj));

        update = new UpdateButtonListener(obj);
        updateButton.addActionListener(update);

        panel.setBorder(BorderFactory.createEmptyBorder(100,0, 100, 0));
        panel.add(addButton);
        panel.add(deleteButton);
        panel.add(updateButton);

        mainPanel.add(panel);

        mainPanel.add(table);
        scrollPane = new JScrollPane(table);
        mainPanel.add(scrollPane);

        frame.add(mainPanel);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void actionPerformed(ActionEvent e) {
        frame.setVisible(true);
    }



    private Client createClientToModify(int rowToModify){
        Client c = new Client();

        int row;
        if (rowToModify == -1){
            row = table.getRowCount() - 1;
        }
        else{
            row = rowToModify;
            c.setClient_id((Integer) table.getValueAt(row, 0));
        }

        c.setFirst_name((String) table.getValueAt(row, 1));
        c.setLast_name((String) table.getValueAt(row, 2));
        c.setEmail((String) table.getValueAt(row, 3));
        c.setPhone((String) table.getValueAt(row, 4));
        c.setAdress((String) table.getValueAt(row, 5));

        return c;
    }

    private Product createProductToModify(int rowToModify){
        Product p = new Product();

        int row;
        if (rowToModify == -1){
            row = table.getRowCount() - 1;
        }
        else{
            row = rowToModify;
            p.setProduct_id((Integer) table.getValueAt(row, 0));
        }

        p.setName((String) table.getValueAt(row, 1));
        p.setPrice(Float.parseFloat("" + table.getValueAt(row, 2)));
        p.setQuantity(Integer.parseInt(""  + table.getValueAt(row, 3)));

        return p;
    }



    class AddButtonListener implements ActionListener {
        private Object obj;

        public AddButtonListener(Object obj){
            this.obj = obj;
        }

        public void actionPerformed(ActionEvent e) {
            if (this.obj instanceof Client){
                new ClientBLL().insert(createClientToModify(-1));
            }
            else if (this.obj instanceof Product){
                new ProductBLL().insert(createProductToModify(-1));
            }

            mainPanel.remove(scrollPane);
            mainPanel.remove(table);

            if (this.obj instanceof Client){
                table = new DatabaseTable<Client>().createTable(new ClientBLL().showAll());
            }
            else if (this.obj instanceof Product){
                table = new DatabaseTable<Product>().createTable(new ProductBLL().showAll());
            }

            mainPanel.add(table);
            scrollPane = new JScrollPane(table);
            mainPanel.add(scrollPane);

            mainPanel.revalidate();
        }
    }

    class DeleteButtonListener implements ActionListener{
        private Object obj;

        public DeleteButtonListener(Object obj){
            this.obj = obj;
        }

        public void actionPerformed(ActionEvent e) {
            if (this.obj instanceof Client){
                new ClientBLL().delete((Integer) table.getValueAt(table.getSelectedRow(), 0));
            }
            else if (this.obj instanceof Product){
                new ProductBLL().delete((Integer) table.getValueAt(table.getSelectedRow(), 0));
            }

            mainPanel.remove(scrollPane);
            mainPanel.remove(table);

            if (this.obj instanceof Client){
                table = new DatabaseTable<Client>().createTable(new ClientBLL().showAll());
            }
            else if (this.obj instanceof Product){
                table = new DatabaseTable<Product>().createTable(new ProductBLL().showAll());
            }

            mainPanel.add(table);
            scrollPane = new JScrollPane(table);
            mainPanel.add(scrollPane);

            mainPanel.revalidate();
        }
    }

    class DoneUpdateButtonListener implements ActionListener{
        private Object obj;
        private int rowToUpdate;

        public DoneUpdateButtonListener(Object obj, int rowToUpdate){
            this.obj = obj;
            this.rowToUpdate = rowToUpdate;
        }

        public void actionPerformed(ActionEvent e) {
            if (this.obj instanceof Client){
                new ClientBLL().update(createClientToModify(rowToUpdate));
            }
            else if (this.obj instanceof Product){
                new ProductBLL().update(createProductToModify(rowToUpdate));
            }

            mainPanel.remove(scrollPane);
            mainPanel.remove(table);

            if (this.obj instanceof Client){
                table = new DatabaseTable<Client>().createTable(new ClientBLL().showAll());
            }
            else if (this.obj instanceof Product){
                table = new DatabaseTable<Product>().createTable(new ProductBLL().showAll());
            }

            mainPanel.add(table);
            scrollPane = new JScrollPane(table);
            mainPanel.add(scrollPane);

            mainPanel.revalidate();

            updateButton.setText("Update Row");

            updateButton.removeActionListener(doneUpdate);
            updateButton.addActionListener(update);

            addButton.setEnabled(true);
            deleteButton.setEnabled(true);

            if (rowToUpdate != -1){
                for (int i = 0; i < table.getColumnCount(); i++){
                    ((MyTableModel) table.getModel()).setCellEditable(rowToUpdate, i, false);
                }
            }
        }
    }

    class UpdateButtonListener implements ActionListener{
        private Object obj;

        public UpdateButtonListener(Object obj){
            this.obj = obj;
        }

        public void actionPerformed(ActionEvent e) {
            updateButton.setText("Press again here when done");

            addButton.setEnabled(false);
            deleteButton.setEnabled(false);

            int rowToUpdate = table.getSelectedRow();

            if (rowToUpdate != -1){
                for (int i = 0; i < table.getColumnCount(); i++){
                    ((MyTableModel) table.getModel()).setCellEditable(rowToUpdate, i, true);
                }
            }
            else{
                System.out.println("Please select a row first!");
            }

            updateButton.removeActionListener(update);

            doneUpdate = new DoneUpdateButtonListener(obj, rowToUpdate);
            updateButton.addActionListener(doneUpdate);
        }
    }

}
