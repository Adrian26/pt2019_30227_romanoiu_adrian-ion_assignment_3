package Presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.lang.reflect.Field;
import java.util.List;


public class DatabaseTable<T> {

    public JTable createTable(List<T> tableData){
        int noRows = tableData.size() + 1;

        int noColumns;
        if (tableData.size() == 0) {
            noColumns = 0;
        } else {
            noColumns = tableData.get(0).getClass().getDeclaredFields().length;
        }

        String[] columnNames = new String[noColumns];
        Object[][] rowsData = new Object[noRows][noColumns];

        int columnIndex = 0;
        int rowIndex = 0;

        if (noColumns != 0) {
            for (Field f : tableData.get(0).getClass().getDeclaredFields()) {
                columnNames[columnIndex] = f.getName();
                columnIndex++;
            }
        }

        columnIndex = 0;
        for (Object obj : tableData) {
            for (Field f : obj.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                try {
                    rowsData[columnIndex][rowIndex] = f.get(obj);
                    rowIndex++;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            columnIndex++;
            rowIndex = 0;
        }

        DefaultTableModel model = new MyTableModel(rowsData, columnNames);
        JTable table = new JTable();
        table.setModel(model);

        for (int i = 0; i < columnNames.length; i++){
            ((MyTableModel) model).setCellEditable(rowsData.length - 1, i, true);
        }

        return table;
    }

}
