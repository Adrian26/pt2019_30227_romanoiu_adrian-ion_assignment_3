package Presentation;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel {

    private boolean[][] editableCells;

    public MyTableModel(Object[][] rowsData, String[] columnNames){
        super(rowsData, columnNames);
        this.editableCells = new boolean[rowsData.length][columnNames.length];

        for (int i = 0; i < rowsData.length; i++){
            for (int j = 0; j < columnNames.length; j++){
                setCellEditable(i, j, false);
            }
        }
    }

    @Override
    public boolean isCellEditable(int row, int column){
        return this.editableCells[row][column];
    }

    public void setCellEditable(int row, int column, boolean value){
        this.editableCells[row][column] = value;
        this.fireTableCellUpdated(row, column);
    }

}
