package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View{

    private JButton clientsButton = new JButton("Clients");
    private JButton productsButton = new JButton("Products");
    private JButton ordersButton = new JButton("Orders");
    private JLabel  textLabel = new JLabel("Store database:");

    public View(){
        JFrame mainFrame = new JFrame("StoreDB");

        JPanel mainPanel = new JPanel();
        JPanel centeredPanel = new JPanel();

        centeredPanel.setLayout(new GridBagLayout());
        centeredPanel.add(textLabel);

        GridLayout myGridLayout = new GridLayout(5, 1);
        myGridLayout.setVgap(25);

        mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 50));
        mainPanel.setLayout(myGridLayout);
        mainPanel.add(centeredPanel);
        mainPanel.add(clientsButton);
        mainPanel.add(productsButton);
        mainPanel.add(ordersButton);

        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        mainFrame.setSize(600, 400);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void addClientButtonListener(ActionListener a){
        clientsButton.addActionListener(a);
    }

    public void addProductButtonListener(ActionListener a) {productsButton.addActionListener(a);}

    public void addOrderButtonListener(ActionListener a) {ordersButton.addActionListener(a);}
}
