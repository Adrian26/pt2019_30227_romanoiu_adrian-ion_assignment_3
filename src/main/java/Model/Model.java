package Model;

import BLL.OrdersBLL;
import BLL.ProductBLL;
import DAO.ClientDAO;
import DAO.OrdersDAO;
import DAO.ProductDAO;
import Presentation.AbstractButtonListener;
import Presentation.DatabaseTable;
import Presentation.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class Model {

    private View view;

    public Model(){
        this.view = new View();

        view.addClientButtonListener(new ClientsButtonListener());
        view.addProductButtonListener(new ProductButtonListener());
        view.addOrderButtonListener(new OrderButtonListener());
    }

    private class ClientsButtonListener extends AbstractButtonListener{
        private ClientsButtonListener(){
            super(new Client());
        }
    }

    private class ProductButtonListener extends AbstractButtonListener{
        private ProductButtonListener(){ super(new Product()); }
    }

    private class OrderButtonListener implements ActionListener {
        private JFrame frame;

        private JButton newOrderButton = new JButton("New Order");
        private JButton deleteButton = new JButton("Delete");

        private JPanel mainPanel = new JPanel(new GridLayout(2, 1));
        private JPanel panel = new JPanel(new GridLayout(1, 3));

        private JScrollPane scrollPane;

        private JTable table;

        public void actionPerformed(ActionEvent e) {
            this.frame = new JFrame("Orders");

            newOrderButton.addActionListener(new NewOrderButtonListener());
            deleteButton.addActionListener(new DeleteOrderButtonListener());

            table = new DatabaseTable<OrdersDAO>().createTable(new OrdersDAO().showAll());

            panel.setBorder(BorderFactory.createEmptyBorder(100,0, 100, 0));
            panel.add(newOrderButton);
            panel.add(deleteButton);

            mainPanel.add(panel);

            mainPanel.add(table);
            scrollPane = new JScrollPane(table);
            mainPanel.add(scrollPane);

            frame.add(mainPanel);
            frame.setVisible(true);
            frame.setSize(800, 600);
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        }


        private class NewOrderButtonListener implements ActionListener{
            JFrame orderFrame = new JFrame("Chose Items...");
            JButton doneButton = new JButton("Done");

            JTable clientsTable = new JTable();
            JTable productsTable = new JTable();

            JPanel mainPanel = new JPanel(new GridLayout(3,1));
            JPanel tablePanel = new JPanel(new GridLayout(1, 2));

            JTextArea textArea = new JTextArea("Select your products by writing: <product_id>-<quantity> here!");

            public void actionPerformed(ActionEvent e) {
                doneButton.addActionListener(new DoneButtonListener());

                clientsTable = new DatabaseTable<Client>().createTable(new ClientDAO().showAll());

                productsTable = new DatabaseTable<Product>().createTable(new ProductDAO().showAll());

                tablePanel.add(clientsTable);
                tablePanel.add(productsTable);

                JScrollPane scrollPane1 = new JScrollPane(clientsTable);
                JScrollPane scrollPane2 = new JScrollPane(productsTable);

                tablePanel.add(scrollPane1);
                tablePanel.add(scrollPane2);

                mainPanel.add(tablePanel);
                mainPanel.add(textArea);
                mainPanel.add(doneButton);

                orderFrame.add(mainPanel);
                orderFrame.setVisible(true);
                orderFrame.setSize(800, 600);
                orderFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            }

            private class DoneButtonListener implements ActionListener{

                public void actionPerformed(ActionEvent e) {
                    int clientID =(Integer) clientsTable.getValueAt(clientsTable.getSelectedRow(), 0);

                    String products = textArea.getText();

                    if ("".equals(products)){
                        System.out.println("No items selected!");
                    }
                    else{
                        String[] results = products.split("\\s");

                        new OrdersBLL().insert(clientID, results);

                        mainPanel.remove(scrollPane);
                        mainPanel.remove(table);

                        table = new DatabaseTable<OrdersDAO>().createTable(new OrdersBLL().showAll());

                        mainPanel.add(table);
                        scrollPane = new JScrollPane(table);
                        mainPanel.add(scrollPane);

                        String bill = "Client " + clientsTable.getValueAt(clientsTable.getSelectedRow(), 1)
                                + " bought: ";

                        float totalPrice = 0;
                        for (String s : results){
                            String[] items = s.split("-");

                            List<Product> product = new ProductBLL().findById(Integer.parseInt(items[0]));

                            bill += product.get(0).getName() + " ";
                            totalPrice += product.get(0).getPrice() * Integer.parseInt(items[1]);
                        }

                        bill += "\n Total price: " + totalPrice;

                        PrintWriter writer = null;
                        try {
                            writer = new PrintWriter("bill.txt", "UTF-8");
                        } catch (FileNotFoundException ex) {
                            ex.printStackTrace();
                        } catch (UnsupportedEncodingException ex) {
                            ex.printStackTrace();
                        }
                        writer.println(bill);
                        writer.close();
                    }
                }
            }
        }

        private class DeleteOrderButtonListener implements ActionListener{

            public void actionPerformed(ActionEvent e) {
                new OrdersBLL().delete((Integer) table.getValueAt(table.getSelectedRow(), 0));

                mainPanel.remove(scrollPane);
                mainPanel.remove(table);

                table = new DatabaseTable<OrdersDAO>().createTable(new OrdersBLL().showAll());

                mainPanel.add(table);
                scrollPane = new JScrollPane(table);
                mainPanel.add(scrollPane);

                mainPanel.revalidate();
            }
        }

    }

}
